//------------------ CLASSE POUR INSTANCIER CHAQUE ÉLÉMENT DE DESSIN----------------- //
/**
 * @description Creates a new Pacman
 */
class Pacman {
  static get go_in_dir()      { return 10; }
  static get laps_to()        { return 100; }
  static get size_start()     { return 50; }
  static get rot_start()      { return 0; }
  static get open_start()     { return PI/12; }
  static get BurstFireSpeed() { return 45; }

  static get decrease_size_badguy_collision() { return 5; }

  static get pos_start()  { return {x: canvas.width/2, y: canvas.height/2}; }

  static get animations() { 
    return {
      badguys:{
        dead : animations.badguys.dead,
        hit  : animations.badguys.hit,
      }
    };
  }

  static get colors() { return {
      body: [{stop: 0, color: 'orange'}, {stop: 1, color: 'red'}],
      eyes: 'black'
    };
  }
  static get badguysParams(){
    return {
      rnd_to_pos_min  : 40,
      rnd_to_pos_max  : 80,
      size_start      : 50,
      size_min        : 8,
      open_start      : PI/12,
      colors          : {
          body: [{stop: 0, color: 'red'}, {stop: 1, color: 'black'}],
          eyes: 'black'
      },
    }
  }

  static makeBadGuys(level){
    for(let i = 0; i < 10 + (level-1)*5; i++){
      new this({
        pos        : {x: parseInt(rnd() * canvas.width), y: parseInt(rnd() * canvas.height)},
        size       : Pacman.badguysParams.size_start,
        rot        : parseInt(rnd() * two_pi),
        open       : Pacman.badguysParams.open_start,
        colors     : Pacman.badguysParams.colors,
        type       : 'badguy',
        sprite     : sprites.spacecraft.badguy,
      });
    }
  }

  static dealBadGuys(){
    for(let i = 0; i < this.badguys.length; i++){
      let badguy = this.badguys[i];
      badguy.randomMoveToPacMan(400/(badguy.size * badguy.size));
      this.hero.badguyCollision(badguy);
      badguy.barriersCollision();
      if(this.badguys.length > 3){ badguy.badguyBreeding(); }
      ctx.drawCharacter(badguy);
      if(this.badguys.length > 3 && badguy.lapsToPacman%5==0){ badguy.askReproductor(); }
      if(badguy.lapsToPacman%250==0){
        badguy.throwBallInBadGuyDir(rndItemInArray(this.badguys));
      }
    }
  }

  static removeDeadBadsGuys(){
    this.badguys.forEach((badguy, i) => { 
      if(badguy.size <= Pacman.badguysParams.size_min){
        this.badguysExplosing.push(this.badguys[i]);
        this.badguys.splice(i, 1);
        Pacman.hero.score.enemy++;
      }
    });
  }

  static async badguysExplose(){
    await this.animations.badguys.dead.play(
      {
        arrObjsToAnim    : this.badguysExplosing,
        sounds           : 'playDeadBadguy', 
        snd              : badguyBlowSound, 
        pathsToAnimInObj : ['animations', 'badguys', 'dead', 'animation'],
      }
    );
  }

  static async badguysHit(){
    this.animations.badguys.hit.play(
      {
        arrObjsToAnim    : this.badguysHiting,
        sounds           : false, 
        pathsToAnimInObj : ['animations', 'badguys', 'hit', 'animation'],
      }
    );
  }

  static async badguysAnimations(){
    //this.badguysHit();
    this.badguysExplose();
  }

  static findBadGuyByNum(num){ return this.badguys.find(badguy => badguy.num === num); }

  static setNumToBadguy(){
    let numsBadguys = [];
    this.badguys.forEach(badguy => { numsBadguys.push(badguy.num); });
    return this.badguys.length ? Math.max(...numsBadguys) + 1 : 1;
  }

  static hero             = {};
  static badguys          = [];
  static badguysExplosing = [];
  static badguysHiting    = [];

  static lapsToPacman = 0;

  constructor(options = {}){
    this.num = options.type === 'badguy' ? Pacman.setNumToBadguy() : 0;

    this.type       = options.type ? options.type : 'pacman';
    this.pos        = options.pos;
    this.size       = options.size;
    this.rot        = options.rot;
    this.open       = options.open;
    this.colors     = options.colors;
    this.sprite     = options.sprite ? options.sprite : sprites.spacecraft.hero;
    this.animations = options.animations ? options.animations : Pacman.animations;

    this.BurstFireSpeed = Pacman.BurstFireSpeed;

    Pacman.lapsToPacman += round(rnd() * Pacman.laps_to, 0);

    this.lapsToPacman        = Pacman.lapsToPacman;
    this.randomAngleToPacman = 0;
    this.ballsCollisions     = 0;
    this.badguysCollisions   = 0;
    this.timeInBarrier       = 0;
    this.score               = new Score();

    this.bonus          = [];
    this.barriersCollid = [];
    this.reproductor    = {asked: -1, askBy: -1, nbAsk: 0, same: false};

    this.type === 'badguy' ? Pacman.badguys.push(this) : Pacman.hero = this;
  }

  distToBadGuy(badguy){
    return h(this.pos.x - badguy.pos.x, this.pos.y - badguy.pos.y);
  }

  badguyCollision(badguy){
    if(this.distToBadGuy(badguy) < this.size){
      this.badguysCollisions++;
      this.size-=Pacman.decrease_size_badguy_collision;
      badguy.rndToPos(Pacman.badguysParams.rnd_to_pos_min, Pacman.badguysParams.rnd_to_pos_max);
      badguy.size++;

      Sounds.playCollidWithBadguySound;
    }
  }

  askReproductor(){
    if(!this.reproductor.same){
      let badguy = rndItemInArray(Pacman.badguys);

      while(badguy === this){ badguy = rndItemInArray(Pacman.badguys); }

      badguy.reproductor.askBy = this.num;
      this.reproductor.asked   = badguy.num;

      this.reproductor.same = this.reproductor.asked === this.reproductor.askBy &&
      this.reproductor.asked !== -1 && this.distToBadGuy(badguy) < this.size;
    }
  }

  badguyBreeding(){
    if(this.reproductor.same){
      let badguyToBreedingWith = Pacman.badguys.find(bg => bg.num === this.reproductor.askBy);

      if(badguyToBreedingWith && this.reproductor.nbAsk === 100){
        let distToBadguy = this.distToBadGuy(badguyToBreedingWith);

        /*if(badguy !== this && distToBadguy < this.size * 2 && this.reproductor.asked === this.reproductor.askBy){
          this.colors.body = [{stop: 0, color: 'pink'}, {stop: 1, color: 'red'}];
        }*/

        if(badguyToBreedingWith !== this && distToBadguy < this.size){
          badguyToBreedingWith.reproductor = { asked: -1, askBy: -1, nbAsk: 0, same: false };
          this.reproductor                 = { asked: -1, askBy: -1, nbAsk: 0, same: false };

          let newColors = deepCopy(Pacman.badguysParams.colors);
          newColors.body = [{stop: 0, color: 'purple'}, {stop: 1, color: 'aqua'}];
          for(let i = 0; i < (2 + 2*(level-1)); i++){
            new Pacman({
              pos     : {x: parseInt(rnd() * canvas.width), y: parseInt(rnd() * canvas.height)},
              size   : Pacman.badguysParams.size_start / 2,
              rot    : parseInt(rnd() * two_pi),
              open   : Pacman.badguysParams.open_start,
              colors : newColors,
              type   : 'badguy',
              sprite : sprites.spacecraft.badguy,
            });
          }
        }
      }
      else if(badguyToBreedingWith){
        this.reproductor.nbAsk++;
        this.size *= (this.reproductor.nbAsk%2==0 ? 2 : 1/2);

        this.rot += rad * 3.6;

        let center = canvas.getCenter();
        canvasMsg([
          {
            pos  :  {x: center.x - 150, y: center.y - 30},
            font : "48px Comic Sans MS",
            text : this.reproductor.nbAsk,
          },
        ]);
      }
    }
  }

  barriersCollision(){
    for(let i = 0; i < Barrier.barriers.length; i++){
      let barrier = Barrier.barriers[i];
      if(this.isCollidBarrier(barrier)){
        if(this !== Pacman.hero){
          let addVect = this.addDirection(-this.rot + quart_pi, (barrier.sizes.w + barrier.sizes.h)/2);
          this.pos.x -= addVect.x;
          this.pos.y -= addVect.y;
        }
        else{
          if(!this.barriersCollid[barrier.num]){ this.barriersCollid[barrier.num]  = {num: barrier.num}; }

          if(!this.barriersCollid[barrier.num].timeAtBarrier){ this.barriersCollid[barrier.num].timeAtBarrier = getTime(); }
          this.barriersCollid[barrier.num].timeInBarrier = getTime() - this.barriersCollid[barrier.num].timeAtBarrier;
          //console.log(this.whatBarrierSide(barrier));
          return this.whatBarrierSide(barrier);
        }
        break;
      }
      else if(this === Pacman.hero && this.barriersCollid[barrier.num]){
        this.barriersCollid[barrier.num].timeAtBarrier = 0;
        this.barriersCollid[barrier.num].timeInBarrier = 0;
        this.barriersCollid[barrier.num].num           = barrier.num;
      }
    }
    return false;
  }

  isCollidBarrier(barrier){
    return this.pos.x + this.size >= barrier.pos.x1 && this.pos.x <= barrier.pos.x2 &&
           this.pos.y + this.size >= barrier.pos.y1 && this.pos.y <= barrier.pos.y2;
  }

  whatBarrierSide(barrier){
    // Collision détectée, déterminer quel côté de rectangle est touché
    const dxLeft   = Math.abs(this.pos.x - barrier.pos.x);
    const dxRight  = Math.abs(this.pos.x - barrier.pos.x2);
    const dyTop    = Math.abs(this.pos.y - barrier.pos.y);
    const dyBottom = Math.abs(this.pos.y - barrier.pos.y2);

    const minSide = Math.min(dxLeft, dxRight, dyTop, dyBottom);

    if(minSide === dxLeft) {  return 'left';   }
    if(minSide === dxRight){  return 'Right';  }
    if(minSide === dyTop)  {  return 'Top';    }
    if(minSide === dyBottom){ return 'Bottom'; }
  }

  rndToPos(dMin, dMax){
    this.pos.x += getRandomIntInclusive(dMin, dMax);
    this.pos.y += getRandomIntInclusive(dMin, dMax);
  }

  addBonusBall(dir, dec){
    this.bonus.filter(bn => bn.type === 'multiballs').forEach((_bn, i) => {
      let k = i%2==0 ? 1 : -1;
      let n = i%2==0 ? i : i-1;

      let vect =  this.addDirection(k*half_pi + dir, (n+1) * this.size/5);
      new Ball({
        pos           : {x: this.pos.x + dec.x + vect.x, y: this.pos.y + dec.y + vect.y},
        direction     : dir,
        size          : Ball.pacman_size * this.sizeOnSizeStart(),
        color         : Ball.from_pacman_color,
        type          : 'toPacManFromBadGuy',
        shooter       : this,
        ellipse_coeff : Ball.ellipse_coeff.pacman,
      });
    });
  }

  isInBarrier(barrier, margin){
    return this.pos.x + margin >= barrier.pos.x1 && this.pos.x + margin <= barrier.pos.x2 && this.pos.y + margin >= barrier.pos.y1 && this.pos.y  + margin <= barrier.pos.y2;
  }

  isInBarriers(){
    return Barrier.barriers.some(barrier => this.isInBarrier(barrier, 0));
  }

  dimSizeInBarriers(){
    this.barriersCollid.forEach(barrierCollid => { if(barrierCollid.timeInBarrier > 1000){ this.size--; } });
  }

  throwBallInPacmanDir(){
    if(!this.isInBarriers()){
      let decToSpacecraft = this.dec(this.rot);
      new Ball({
        pos           : {x: this.pos.x + decToSpacecraft.x, y: this.pos.y + decToSpacecraft.y},
        direction     : this.rot,
        size          : Ball.pacman_size * this.sizeOnSizeStart(),
        color         : Ball.from_pacman_color,
        ellipse_coeff : Ball.ellipse_coeff.pacman
      });
      this.addBonusBall(this.rot, decToSpacecraft);
      Sounds.playBallSound;
    }
  }

  async throwBallInMouseDir(){
    if(!this.isInBarriers()){
      let decToSpacecraft = this.dec();
      let dir = atan2piZ(mouse.x - this.pos.x, mouse.y - this.pos.y);
      new Ball({
        pos           : {x: this.pos.x + decToSpacecraft.x, y: this.pos.y + decToSpacecraft.y},
        direction     : dir,
        size          : Ball.pacman_size * this.sizeOnSizeStart(),
        color         : Ball.from_pacman_color,
        type          : 'toPacManFromBadGuy',
        shooter       : this,
        ellipse_coeff : Ball.ellipse_coeff.pacman,
      });
      this.addBonusBall(dir, decToSpacecraft);
      Sounds.playBallSound;
    }
  }

  throwBallInBadGuyDir(badguy){
    if(!this.isInBarriers()){
      let decToSpacecraft = this.dec();
      if(this !== badguy){
        let dir = atan2piZ(badguy.pos.x - this.pos.x, badguy.pos.y - this.pos.y);
        new Ball({
          pos           : {x: this.pos.x + decToSpacecraft.x, y: this.pos.y + decToSpacecraft.y},
          direction     : dir,
          size          : Ball.badguy_size * this.sizeOnSizeStart(),
          color         : Ball.from_badguy_color,
          type          : 'toBadGuyfromBadGuy',
          shooter       : badguy,
          ellipse_coeff : Ball.ellipse_coeff.badguy,
        });
        this.addBonusBall(dir, decToSpacecraft);
        Sounds.playBallSound;
      }
    }
  }

  dec(dir = 0){
    if(dir <= half_pi){ return this.addDirection(quart_pi, this.size * SQRT1_2); }
    if(dir < PI){ return this.addDirection(-quart_pi, this.size * SQRT1_2); }
    if(dir <= 1.5*PI){ return this.addDirection(quart_pi, this.size * SQRT1_2); }
    else{ return this.addDirection(quart_pi, this.size * SQRT1_2); }
  }

  sizeOnSizeStart(){
    return  this.size / (this.type === 'badguy' ? Pacman.badguysParams.size_start : Pacman.size_start);
  }

  goInDir(angle, dist){
    this.pos.x += cos(angle) * dist;
    this.pos.y += sin(angle) * dist;

    this.rot = angle;
  }

  randomMoveToPacMan(dist){
    let pacM = this.reproductor.same ?
    Pacman.badguys.find(badguy => badguy.num === this.reproductor.asked) : Pacman.hero;

    if(Pacman.badguys.length < 4){ pacM = Pacman.hero; }
    
    if(!pacM){ pacM = Pacman.hero; }

    if(this.lapsToPacman%100==0){ this.randomAngleToPacman = rnd(); }

    let angleToPacMan    = atan2piZ(pacM.pos.x - this.pos.x, pacM.pos.y - this.pos.y);
    let newAngleToPacMan = angleToPacMan /*+ this.randomAngleToPacman*/;
    let dir              = this.addDirection(newAngleToPacMan, dist * this.reproductor.same ? 2 : 1);

    this.addVect(dir);

    this.rot = newAngleToPacMan;
    if(this.reproductor.same && Pacman.badguys.length > 3){
      this.size *= (this.lapsToPacman%2==0 ? 4 : 1/4);
      this.rot += rad * 3.6;
    }

    this.lapsToPacman++;
  }

  addDirection(angle, dist){
    return {
      x:  cos(angle) * dist,
      y:  sin(angle) * dist
    };
  }

  addVect(v){ this.pos.x += v.x;  this.pos.y += v.y}
}

/**
 * @description Creates a new Ball
 */
class Ball {
  static get nbBallsMax(){ return 3000; }
  static get speed(){ return 10; }
  static get size_min_to_badguy(){ return 5; }
  static get pacman_size(){ return 15; }
  static get badguy_size(){ return 10; }
  static get nbBouncesLim() { return 10; }
  static get from_pacman_color(){ return [{stop: 0, color: 'hsla(130, 70%, 70%, 1)'}, {stop: 0.5, color: 'hsla(130, 70%, 40%, 1)'}, {stop: 1, color: 'hsla(130, 70%, 10%, 1)'}]; }
  static get from_badguy_color(){ return [{stop: 0, color: 'hsla(30, 70%, 70%, 1)'}, {stop: 0.5, color: 'hsla(30, 70%, 40%, 1)'}, {stop: 1, color: 'hsla(30, 70%, 40%, 1)'}]; }
  static get ellipse_coeff(){ return {pacman: 3, badguy: 2}; }

  static balls = [];

  static killOutBalls(){
    this.balls.forEach((ball, i) => {
      if(ball.isInBarriers() || ball.toKill || ball.isUpperNbBouncesLim() ||
      (ball.pos.x < 0 || ball.pos.y < 0 || ball.pos.x > canvas.width || ball.pos.y > canvas.height)){
        this.balls.splice(i, 1);
      }
    });
    let limBall = this.nbBallsMax - 1;
    if(this.balls.length >= limBall){ this.balls.splice(limBall, this.balls.length - limBall); }
  }

  isInBarrier(barrier, margin){
    return this.pos.x + margin >= barrier.pos.x1 && this.pos.x + margin <= barrier.pos.x2 && this.pos.y + margin >= barrier.pos.y1 && this.pos.y  + margin <= barrier.pos.y2;
  }

  isInBarriers(){
    return Barrier.barriers.some(barrier => this.isInBarrier(barrier, 0));
  }

  isInBarrierCorner(barrier, cornerSize = {inner: 30, outer: 30}){
    let corners = barrier.getCorners(cornerSize);

    let xP = this.pos.x;
    let yP = this.pos.y;
    let x1 = corners.leftTop.outerPoint.x;
    let y1 = corners.leftTop.outerPoint.y;
    let x2 = corners.leftTop.innerPoint.x;
    let y2 = corners.leftTop.innerPoint.y;
    if(xP >= x1 && xP <= x2 && yP >= y1 && yP <= y2){ return true; }

    x1 = corners.rightTop.outerPoint.x;
    y1 = corners.rightTop.outerPoint.y;
    x2 = corners.rightTop.innerPoint.x;
    y2 = corners.rightTop.innerPoint.y;
    if(xP <= x1 && xP >= x2 && yP >= y1 && yP <= y2){ return true; }

    x1 = corners.rightBottom.outerPoint.x;
    y1 = corners.rightBottom.outerPoint.y;
    x2 = corners.rightBottom.innerPoint.x;
    y2 = corners.rightBottom.innerPoint.y;
    if(xP <= x1 && xP >= x2 && yP <= y1 && yP >= y2){ return true; }

    x1 = corners.leftBottom.outerPoint.x;
    y1 = corners.leftBottom.outerPoint.y;
    x2 = corners.leftBottom.innerPoint.x;
    y2 = corners.leftBottom.innerPoint.y;
    if(xP >= x1 && xP <= x2 && yP <= y1 && yP >= y2){ return true; }

    return false;
  }

  static collisionBalls(){
    Pacman.badguys.forEach(badguy => { 
      this.balls.forEach(ball => { ball.badguyCollision(badguy); });
    });
    this.balls.filter(ball => ball.type === 'toBadGuyfromBadGuy').forEach(ball => { ball.badguyCollision(Pacman.hero); });
    this.balls.forEach(ball => {
      if(!this.isCollid){
        ball.barriersCollision();
      }
      else{
        this.nbChecksBounces++;
        if(this.nbChecksBounces > Ball.nbBouncesLim){ this.isCollid = false; }
      }
      
      ball.bonusCollision();
    });
  }

  static dealBalls(){
    this.balls.forEach(ball => {
      let speedBonus = 1;
      ball.shooter.bonus.forEach(bn => { speedBonus *= bn.type === 'speed' ? 1.2 : 1 });
      ball.goInDir(ball.direction, ball.speed * speedBonus);
      ctx.drawBall(ball);
    });
  }

  static setNumToBall(){
    let numsBalls = [];
    this.balls.forEach(ball => { numsBalls.push(ball.num); });
    return this.balls.length ? Math.max(...numsBalls) + 1 : 1;
  }

  constructor(options = {}){
    this.num = Ball.setNumToBall();

    this.pos           = options.pos;
    this.direction     = options.direction;
    this.size          = options.size;
    this.color         = deepCopy(options.color);
    this.type          = options.type ? options.type : 'toPacManFromBadGuy';
    this.shooter       = options.shooter ? options.shooter : Pacman.hero;
    this.speed         = options.speed ? options.speed : Ball.speed;
    this.ellipse_coeff = options.ellipse_coeff;
    this.sprite        = !options.sprite ?  (this.shooter.type === 'pacman' ? sprites.ball.hero : sprites.ball.badguy) : options.sprite,

    this.rot      = false;
    this.toKill   = false;
    this.isCollid = false;
    this.invDir   = {x: false, y: false};

    this.nbBounces       = 0;
    this.nbChecksBounces = 0;

    Ball.balls.push(this);
  }

  distToBadGuy(badguy){
    return h(this.pos.x - badguy.pos.x, this.pos.y - badguy.pos.y);
  }

  badguyCollision(badguy){
    if(this.shooter !== badguy && this.distToBadGuy(badguy) < badguy.size){
      badguy.ballsCollisions++;
      
      if(this.type === 'toPacManFromBadGuy'){
        badguy.size-=5;
        Pacman.hero.size++;
        Pacman.hero.score.points++;
      }
      else{
        badguy.size--;
        this.shooter.size++;
      }

      Pacman.badguysHiting[0] = badguy;
      for(let i = 0; i < Ball.balls.length; i++){
        if(Ball.balls[i] === this){ Ball.balls.splice(i, 1); break; }
      }

      Sounds.playBallCollisionSound;
    }
  }

  isCollidBarrier(barrier){
    return this.pos.x >= barrier.pos.x1 && this.pos.x <= barrier.pos.x2 && this.pos.y >= barrier.pos.y1 && this.pos.y <= barrier.pos.y2;
  }

  isCollidBonus(bonus){
    return this.pos.x >= bonus.pos.x1 && this.pos.x <= bonus.pos.x2 && this.pos.y >= bonus.pos.y1 && this.pos.y <= bonus.pos.y2;
  }

  isUpperNbBouncesLim(){
    return this.nbBounces > Ball.nbBouncesLim; 
  }

  barriersCollision(){
    this.isCollid = false;
    this.invDir.x = false;
    this.invDir.y = false;
    Barrier.barriers.forEach(barrier => {
      if (!this.isInBarrierCorner(barrier, {inner: this.size, outer: this.size}) &&
      this.pos.x + this.size >= barrier.pos.x && this.pos.x - this.size/4 <= barrier.pos.x2) {

          if (this.pos.y + this.size >= barrier.pos.y && this.pos.y - this.size/4 <= barrier.pos.y2) {
            
          this.nbBounces++;
            
          // Collision détectée, déterminer quel côté de rectangle est touché
          const dxLeft   = Math.abs(this.pos.x - barrier.pos.x);
          const dxRight  = Math.abs(this.pos.x - barrier.pos.x2);
          const dyTop    = Math.abs(this.pos.y - barrier.pos.y);
          const dyBottom = Math.abs(this.pos.y - barrier.pos.y2);

          const minDx = Math.min(dxLeft, dxRight);
          const minDy = Math.min(dyTop, dyBottom);

          if (minDx < minDy) {
            // Rebond horizontal
            this.invDir.x = true;
            this.invDir.y = false;
          }
          else {
            // Rebond vertical
            this.invDir.x = false;
            this.invDir.y = true;
          }

          this.isCollid = true;
        }
        else{ this.nbBounces = 0; }
      }
      else{ this.nbBounces = 0; }
    });
  }

  bonusCollision(){
    for(let i = 0; i < Bonus.bonus.length; i++){
      let bn = Bonus.bonus[i];
      if(this.isCollidBonus(bn)){
        this.shooter.bonus.push(bn);
        Bonus.bonusTaking.push(bn);

        this.shooter.score.bonus++;

        Bonus.bonus.splice(i, 1); i--;

        Sounds.playBallCollisionBonusSound;
      }
    }
  }
  
  goInDir(angle, dist){
    let dx = cos(angle) * dist;
    let dy = sin(angle) * dist;

    if(this.invDir.x){
      dx = -dx;
    }
    else if(this.invDir.y){
      dy = -dy;
    }

    this.rot       = atan2piZ(dx, dy);
    this.direction = this.rot;

    this.toKill = false;
    if(dx+dy === 0){ this.toKill = true; }

    this.pos.x += dx;
    this.pos.y += dy;
  }

  dir(angle, dist){
    return {
      x:  cos(angle) * dist,
      y:  -sin(angle) * dist
    };
  }

  addVect(v){ this.pos.x += v.x;  this.pos.y += v.y}
}

class Barrier{
  static get size_max(){ return 300; }
  static get size_min(){ return 100; }
  static get width(){ return 25; }
  static get nbBricks(){ return 5; }
  static get lineWidth(){ return 2; }
  static get stroke_color(){ return '#1266aa'; }
  static get line_color(){ return 'red'; }
  static get coeff_pos_canvas(){ return 0.95; }
  static get fill_color(){ return [{stop: 0, color: '#40E0D0'}, {stop: 1, color: 'blue'}]; }

  static barriers = [];

  static drawBarriers(){ this.barriers.forEach(barrier => { barrier.draw(); }); }

  static makeBarriers(){
    for(let i = 0; i < (5 + 2*(level-1)); i++){
      let xDec = canvas.width * (1-this.coeff_pos_canvas);
      let yDec = canvas.height * (1-this.coeff_pos_canvas);

      let coeff = this.coeff_pos_canvas - 0.15;

      let x = xDec + parseInt(rnd() * canvas.width * coeff);
      let y = yDec + parseInt(rnd() * canvas.height * coeff);

      let size = getRandomIntInclusive(this.size_min, this.size_max);

      let sizes = i%2==0 ? {w: size, h: this.size_min + size * rnd()} : {w: this.size_min + size * rnd(), h: size};

      if(sizes.w < sizes.h){ sizes.w = this.width; }
      else{ sizes.h = this.width; }

      new this({
        pos    : {x, y},
        sizes  : {w: sizes.w, h: sizes.h},
        colors : {stroke: this.stroke_color, fill: this.fill_color, line: this.line_color},
        sprite : sprites.barrier,
      });
    }
  }

  static setNumToBarrier(){
    let numsBarrier = [];
    this.barriers.forEach(barrier => { numsBarrier.push(barrier.num); });
    return this.barriers.length ? Math.max(...numsBarrier) + 1 : 1;
  }

  constructor(options = {}){
    this.num = Barrier.setNumToBarrier();

    this.pos    = options.pos;
    this.sizes  = options.sizes;
    this.colors = options.colors;
    this.sprite = options.sprite;

    this.pos.x1 = this.pos.x;
    this.pos.x2 = this.pos.x + this.sizes.w;
    this.pos.y1 = this.pos.y;
    this.pos.y2 = this.pos.y + this.sizes.h;

    Barrier.barriers.push(this);
  }

  getCorners(cornersSize){
    return {
      leftTop     :{
        outerPoint: {x: this.pos.x1 - cornersSize.outer, y: this.pos.y1 -  cornersSize.outer},
        innerPoint: {x: this.pos.x1 + cornersSize.inner, y: this.pos.y1 + cornersSize.inner}
      },
      rightTop    :{
        outerPoint: {x: this.pos.x2 + cornersSize.outer, y: this.pos.y1 - cornersSize.outer},
        innerPoint: {x: this.pos.x2 - cornersSize.inner, y: this.pos.y1 + cornersSize.inner}
      },
      rightBottom :{
        outerPoint: {x: this.pos.x2 + cornersSize.outer, y: this.pos.y2 + cornersSize.outer},
        innerPoint: {x: this.pos.x2 - cornersSize.inner, y: this.pos.y2 - cornersSize.inner}
      },
      leftBottom  :{
        outerPoint: {x: this.pos.x1 - cornersSize.outer, y: this.pos.y2 + cornersSize.outer},
        innerPoint: {x: this.pos.x1 + cornersSize.inner, y: this.pos.y2 - cornersSize.inner} 
      },
    };
  }

  draw(){
    ctx.drawBarrier(this);
  }
}

class Bonus{
  static get types() { return [
    {
      type             : 'speed',
      size             : 30,
      coeff_pos_canvas : 0.80,
      distByLap        : 3,
      nbLapsToTurn     : 20,
      sprite           : sprites.bonus.speed,
      colors           : {
          stroke : 'black',
          fill   : [{stop: 0, color: 'purple'}, {stop: 1, color: '#582900'}],
      }, 
    },
    {
      type             : 'size',
      size             : 30,
      coeff_pos_canvas : 0.80,
      distByLap        : 3,
      nbLapsToTurn     : 20,
      sprite           : sprites.bonus.size,
      colors           : {
          stroke : 'black',
          fill   : [{stop: 0, color: 'pink'}, {stop: 1, color: 'red'}],
      },
    },
    {
      type             : 'multiballs',
      size             : 30,
      coeff_pos_canvas : 0.80,
      distByLap        : 3,
      nbLapsToTurn     : 20,
      sprite           : sprites.bonus.multiballs,
      colors           : {
          stroke : 'black',
          fill   : [{stop: 0, color: 'green'}, {stop: 1, color: 'red'}],
      },
    }
  ];}

  static bonus       = [];
  static bonusTaking = [];

  static get animations() { 
    return {
      bonus:{
        taken : animations.bonus.taken,
      }
    };
  }

  static dealBonus(){
    this.bonus.forEach(bn => {
      bn.rndMove(bn.distByLap, bn.nbLapsToTurn);
      ctx.drawBonus(bn);
    });
  }

  static makeBonus(level){
    for(let i = 0; i < (3 + 2*(level-1)); i++){
      let bn = Bonus.types[round(rnd() * (Bonus.types.length-1), 0)];
      new Bonus({
        pos          : {x: parseInt(rnd() * canvas.width * bn.coeff_pos_canvas), y: parseInt(rnd() * canvas.height * bn.coeff_pos_canvas)},
        size         : bn.size,
        type         : bn.type,
        colors       : {stroke: bn.colors.stroke, fill: bn.colors.fill},
        distByLap    : bn.distByLap,
        nbLapsToTurn : bn.nbLapsToTurn,
        sprite       : bn.sprite,
      });
    }
  }

  static async bonusTaken(){
    await this.animations.bonus.taken.play(
      {
        arrObjsToAnim    : this.bonusTaking, 
        snd              : bonusSound, 
        pathsToAnimInObj : ['animations', 'bonus', 'taken', 'animation'],
      }
    );
  }

  static async bonusAnimations(){
    await this.bonusTaken();
  }

  static setNumToBonus(){
    let numsBonus = [];
    this.bonus.forEach(bonus => { numsBonus.push(bonus.num); });
    return this.bonus.length ? Math.max(...numsBonus) + 1 : 1;
  }

  constructor(options = {}){
    this.num = Bonus.setNumToBonus();

    this.type         = options.type;
    this.pos          = options.pos;
    this.size         = options.size;
    this.colors       = options.colors;
    this.distByLap    = options.distByLap;
    this.nbLapsToTurn = options.nbLapsToTurn;
    this.sprite       = options.sprite;
    this.animations   = options.animations ? options.animations : Bonus.animations;

    this.turnPtAndSizeToPts();

    this.laps       = 0;
    this.randomMove = {x: 0, y: 0};

    Bonus.bonus.push(this);
  }

  turnPtAndSizeToPts(){
    this.pos.x1 = this.pos.x;
    this.pos.x2 = this.pos.x + this.size;
    this.pos.y1 = this.pos.y;
    this.pos.y2 = this.pos.y + this.size;
  }

  rndMove(dist, nbLaps){
    if(this.laps%nbLaps==0){
      this.randomMove.x = rnd_sign() * dist;
      this.randomMove.y = rnd_sign() * dist;
    }
    this.pos.x += this.randomMove.x;
    this.pos.y += this.randomMove.y;

    this.turnPtAndSizeToPts();

    this.laps++;
  }
}

class Sounds{
  static get BEAT(){ return 0.33; }
  static get TEMPO() { return {
      doubleCroche : 0.25 * this.BEAT,
      croche       : 0.5  * this.BEAT,
      noire        : 1    * this.BEAT,
      blanche      : 2    * this.BEAT,
      ronde        : 4    * this.BEAT,
    }
  }

  static get ball(){ return new this({frequence: 330, duration: 0.05, gain: 0.25, type: 'triangle'}); }
  static get badguy_collision(){ return new this({frequence: 220, duration: 0.05, gain: 0.25, type: 'sawtooth'}); }
  static get ball_badguy_collision(){ return new this({frequence: 880, duration: 0.05, gain: 0.25, type: 'triangle'}); }
  static get ball_barrier_collision(){ return new this({frequence: 110, duration: 0.05, gain: 0.25, type: 'square'}); }
  static get melody(){ return {
        game_over: new this([
            {frequence: NOTES.octave5.sol, duration: this.TEMPO.croche, gain: 0.5, type: 'sawtooth'},
            {frequence: NOTES.octave5.re, duration: this.TEMPO.noire, gain: 0.5, type: 'sawtooth'},
            {frequence: NOTES.octave5.do, duration: this.TEMPO.croche, gain: 0.5, type: 'sawtooth'},
            {frequence: NOTES.octave5.do, duration: this.TEMPO.noire, gain: 1, type: 'sawtooth'},
        ]),
        game_winner : new this([
            {frequence: NOTES.octave5.do, duration: this.TEMPO.croche, gain: 0.5, type: 'sawtooth'},
            {frequence: NOTES.octave5.mi, duration: this.TEMPO.noire, gain: 0.5, type: 'sawtooth'},
            {frequence: NOTES.octave5.sol, duration: this.TEMPO.croche, gain: 0.5, type: 'sawtooth'},
            {frequence: NOTES.octave6.do, duration: this.TEMPO.noire, gain: 1, type: 'sawtooth'},
        ]),
        ball_bonus_collision : new this([
            {frequence: NOTES.octave5.do, duration: this.TEMPO.croche, gain: 0.5, type: 'triangle'},
            {frequence: NOTES.octave5.mi, duration: this.TEMPO.noire, gain: 0.5, type: 'triangle'},
            {frequence: NOTES.octave5.sol, duration: this.TEMPO.croche, gain: 0.5, type: 'triangle'},
            {frequence: NOTES.octave6.do, duration: this.TEMPO.noire, gain: 0.5, type: 'triangle'},
        ]),
    };
  }
  static get effects(){
    let sounds = [];
    for(let i = 0; i < 20; i++){
      sounds.push({frequence: 110 + round(rnd() * 80, 0), duration: 0.02 + rnd() * 0.02, gain: 1, type: 'triangle', deadDuration: 0});
    }
    return {
    dead_badguy:
      new this(sounds),
    hit_badguy:
      new this([
            {frequence: 880, duration: 0.05, gain: 0.1, type: 'triangle'},
          ]),
    };
  }

  static get playBallSound                 () { this.ball.play(); }
  static get playCollidWithBadguySound     () { this.badguy_collision.play(); }
  static get playBallCollisionSound        () { this.ball_badguy_collision.play(); }
  static get playBallCollisionBarrierSound () { this.ball_barrier_collision.play(); }
  static get playDeadBadguy                () { this.effects.dead_badguy.play(); }
  static get playHitBadguy                 () { this.effects.hit_badguy.play(); }
  static get playBallCollisionBonusSound   () { this.melody.ball_bonus_collision.play(); }
  static get playMelodyGameOver            () { this.melody.game_over.play(); }
  static get playMelodyGameWinner          () { this.melody.game_winner.play(); }

  constructor(notes){
    this.notes = notes;
  }
  async play() {
    if(soundOn){
      if(Array.isArray(this.notes)){
        for(let i = 0; i < this.notes.length; i++){
          await this.playNote(this.notes[i]);
        }
      }
      else{
        await this.playNote(this.notes);
      }
    }
  };
  
  playNote (note) {
     return new Promise((resolve) => {
        let deadDuration = note.deadDuration ? note.deadDuration : 0.01;

        const volume = new GainNode(audioCtx);
        volume.connect(audioCtx.destination);
        volume.gain.setValueAtTime(0, audioCtx.currentTime);
        volume.gain.linearRampToValueAtTime(note.gain, audioCtx.currentTime + deadDuration);
        volume.gain.linearRampToValueAtTime(0, audioCtx.currentTime + note.duration - deadDuration);
  
        const oscillator = new OscillatorNode(audioCtx);
        oscillator.type = note.type;
        oscillator.connect(volume);
        oscillator.frequency.value = note.frequence;
        oscillator.start();
  
        setTimeout(() => {
            oscillator.stop();
            resolve();
        }, note.duration * 1000);
      }
    );
  }
}

class Score{
  constructor(options = {}){
    this.points = options.points ? options.points : 0;
    this.enemy  = options.enemy ? options.enemy : 0;
    this.bonus  = options.bonus ? options.bonus : 0;
  }
}

class spritesAnimation{
  constructor(anim){
    this.animation = anim;
  }

  async play(options = {}){
    if(options.arrObjsToAnim.length){
      let anim             = this.animation;
      let arrObjsToAnim    = options.arrObjsToAnim;
      let sounds           = options.snd ? options.snd : (options.sounds ? options.sounds : false);
      let pathsToAnimInObj = options.pathsToAnimInObj;

      let scale = {
        x: anim.size.w / anim.nbRows,
        y: anim.size.h / anim.nbCols,
      };

      let animInObj;
      arrObjsToAnim.forEach((objToAnim, i) => {
        animInObj = objToAnim[pathsToAnimInObj[0]];
        for(let i = 1; i < pathsToAnimInObj.length; i++){ 
          animInObj = animInObj[pathsToAnimInObj[i]];
        }

        if(animInObj && animInObj.img instanceof Image ){
          if(!objToAnim.explosion){
            arrObjsToAnim[i].explosion = {nbH: 0, nbV: 0};
          }

          ctx.drawAnimation(anim.img, 
            {
              sx       : scale.x * objToAnim.explosion.nbH,
              sy       : scale.y * objToAnim.explosion.nbV,
              sLargeur : scale.x,
              sHauteur : scale.y,
              dx       : objToAnim.pos.x,
              dy       : objToAnim.pos.y,
              dLargeur : scale.x,
              dHauteur : scale.y,
            }
          );

          arrObjsToAnim[i].explosion.nbH++;

          if(sounds && objToAnim.explosion.nbH === 1 && objToAnim.explosion.nbV === 0){
            if(!options.snd){ Sounds[sounds]; }
            else{
              sounds();
            }
          }
          
          if(objToAnim.explosion.nbH === anim.nbRows){ arrObjsToAnim[i].explosion.nbH = 0; arrObjsToAnim[i].explosion.nbV++; }
          if(objToAnim.explosion.nbV === anim.nbCols){ arrObjsToAnim.splice(i, 1); }
        }
        else{
          arrObjsToAnim.splice(i, 1);
        }
      });
    }
  }
}

const animations = {
  badguys:{
      dead : new spritesAnimation(sprites.animations.badguy.dead),
      hit  : new spritesAnimation(sprites.animations.badguy.hit),
  },
  hero:{
      dead : new spritesAnimation(sprites.animations.hero.dead),
  },
  bonus:{
      taken : new spritesAnimation(sprites.animations.bonus.taken),
  },
}















//END CLASSES
