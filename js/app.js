var startWidth  = canvas.width;
var startHeight = canvas.height;

giveFuncToCanvas(canvas, ctx);

//************************************************************************************//
//***************************************Pacman***************************************//
//************************************************************************************//

//------------------ BOUCLE D'ANIMATION ----------------- //
function animation(){
  if(!pause && !game_over){
    clear();

    if(testApp){
      test();
    }
    else if(Pacman.hero.size > 0 && Pacman.badguys.length){
      ctx.drawCharacter();
      Pacman.removeDeadBadsGuys();
      Pacman.badguysAnimations();
      Pacman.hero.barriersCollision();
      Pacman.hero.dimSizeInBarriers();
      if(!stopBadGuy){ Pacman.dealBadGuys(); }
      Ball.killOutBalls();
      Ball.collisionBalls();
      Ball.dealBalls();
      Barrier.drawBarriers();
      Bonus.dealBonus();
      Bonus.bonusAnimations();

      showInfos();

      lapsGame++;
    }
    else if(Pacman.badguys.length){
      game_over = true;

      let center = canvas.getCenter();
      canvasMsg(
        [
          {
            pos  :  {x: center.x - 150, y: center.y - 30},
            font : "48px Comic Sans MS",
            text : "Game over !",
          },
          {
            pos  :  {x: center.x - 300, y: center.y + 30},
            font : "48px Comic Sans MS",
            text : "Appuyer sur q ou escape",
          },
          {
            pos  :  {x: center.x - 250, y: center.y + 90},
            font : "48px Comic Sans MS",
            text : "pour recommencer !",
          },
        ]
      );
      Sounds.playMelodyGameOver;
    }
    else{
      if(!nextLevel){ Sounds.playMelodyGameWinner; }
      nextLevel = true;

      let center = canvas.getCenter();
      canvasMsg(
        [
          {
            pos  :  {x: center.x - 125, y: center.y - 30},
            font : "48px Comic Sans MS",
            text : "You win !",
          },
          {
            pos  :  {x: center.x - 300, y: center.y + 30},
            font : "48px Comic Sans MS",
            text : "Appuyer sur q ou escape",
          },
          {
            pos  :  {x: center.x - 275, y: center.y + 90},
            font : "48px Comic Sans MS",
            text : "pour le niveau suivant !",
          },
          {
            pos  :  {x: center.x - 275, y: center.y + 150},
            font : "48px Comic Sans MS",
            text : "Score : " + score(),
          },
        ]
      );
    }
  }
  requestAnimationFrame(animation);
}

function scoring(){
  let count = 0;

  return function(){
    if(count === Pacman.hero.score.points){ count = 0; }
    return ++count;
  }
}
const score = scoring();

function initLevel(level = 1){
  nextLevel      = false;
  Ball.balls     = [];
  Pacman.badguys = [];

  let scoreSave = new Score();
  if(Pacman.hero.score){ scoreSave = deepCopy(Pacman.hero.score); }

  new Pacman({
    pos    : Pacman.pos_start,
    size   : Pacman.size_start,
    rot    : Pacman.rot_start,
    open   : Pacman.open_start,
    colors : Pacman.colors,
    type   : 'pacman',
  });

  Pacman.hero.score = scoreSave;

  Pacman.makeBadGuys(level);
  Barrier.makeBarriers(level);
  Bonus.makeBonus(level);
}

function showInfos(){
  let livePercent = 100 * Pacman.hero.size / Pacman.size_start;

  canvasMsg(
    [
      {
        pos:  {x: 30, y: 30},
        text: "Score : " + Pacman.hero.score.points + "  -  Enemy : All kills: " + Pacman.hero.score.enemy + ", To kill: " +
        Pacman.badguys.length + "  -  Bonus : All: " + Pacman.hero.score.bonus + ", To take: " + Bonus.bonus.length,
      },
      {
        pos:  {x: 30, y: 60},
        text: "Level : " + level + "  -  Lives : " + livePercent + " %",
      },
    ]
  );
}

//************************************************************************************//
//************************************************************************************//
//************************************************************************************//

/**
 * @description Gives function to a canvas variable and to a ctx variable
 * @param {HTMLCanvasElement} varCanvas The canvas variable
 * @param {CanvasRenderingContext2D} varCtx The ctx variable
 * @returns {void}
 */
function giveFuncToCanvas(varCanvas, varCtx){
  //------------------ GET CENTER OF CANVAS ----------------- //
  varCanvas.getCenter = function(){ return {x: this.width/2, y: this.height/2}; };

  //------------------ DRAW A Pacman ON CANVAS ----------------- //
  varCtx.drawPacman = function(p = Pacman.hero){
    let pos        = p.pos;
    let size       = p.size > 0 ? p.size : 0;
    let rot        = p.rot;
    let open       = p.open;
    let bodyColors = p.colors.body;
    let eyesColor  = p.colors.eyes;

    let startAngle = open + rot;
    let endAngle   = two_pi - (open - rot);

    p.bonus.filter(bn => bn.type === 'size').forEach(_bn => {
      size *= 1.2;
    });

    const gradient = varCtx.createLinearGradient(pos.x, pos.y, pos.x + size, pos.y + size);
    bodyColors.forEach(bodyColor => {
      gradient.addColorStop(bodyColor.stop, bodyColor.color);
    });

    this.beginPath();
    this.fillStyle = gradient;
    this.moveTo(pos.x, pos.y);
    this.arc(pos.x, pos.y, size, startAngle, endAngle, false);
    this.fill();

    let dir    = direction(endAngle - PI/12, size / 1.2);
    let dirEnd = direction(endAngle + PI/4, size / 1.2);

    this.beginPath();
    this.fillStyle = eyesColor;
    this.arc(pos.x + dir.x, pos.y + dir.y, size/10, 0, two_pi, false);
    this.arc(pos.x + dirEnd.x, pos.y + dirEnd.y, size/10, 0, two_pi, false);
    this.fill();
  };
  //------------------ DRAW A Pacman ON CANVAS ----------------- //
  varCtx.drawCharacter = function(p = Pacman.hero){
    let pos  = p.pos;
    let size = p.size > 0 ? p.size : 0;
    let rot  = p.rot;

    let dec = {x: pos.x + size/2, y: pos.y + size/2};

    ctx.translate(dec.x, dec.y);
    ctx.rotate(rot + half_pi);
    ctx.translate(-dec.x, -dec.y);

    varCtx.drawImage(p.sprite, pos.x, pos.y, size, size);
    varCtx.setTransform(1, 0, 0, 1, 0, 0);
  };
  //------------------ DRAW A Pacman ON CANVAS ----------------- //
  varCtx.drawAnimation = function(imgData, options){
    varCtx.drawImage(imgData, options.sx, options.sy, options.sLargeur, options.sHauteur, options.dx, options.dy, options.dLargeur, options.dHauteur);
    varCtx.setTransform(1, 0, 0, 1, 0, 0);
  };
  //------------------ DRAW A Ball ON CANVAS ----------------- //
  varCtx.drawBall = function(ball){
    varCtx.drawImage(ball.sprite, ball.pos.x, ball.pos.y, ball.size, ball.size);
    varCtx.setTransform(1, 0, 0, 1, 0, 0);
  };
  /*varCtx.drawBall = function(ball){
    let pos       = ball.pos;
    let size      = ball.size;
    let colors    = ball.color;

    const gradient = this.createLinearGradient(pos.x, pos.y, pos.x + size, pos.y + size);
    colors.forEach(color => {
      gradient.addColorStop(color.stop, color.color);
    });

    let direction = !ball.rot ? ball.direction : ball.rot;

    this.beginPath();
    this.fillStyle   = gradient;
    this.strokeStyle = 'black';
    this.lineWidth   = 1;
    this.moveTo(pos.x, pos.y);
    this.ellipse(pos.x, pos.y, size/ball.ellipse_coeff, size, -half_pi + direction, 0, two_pi);
    //this.arc(pos.x, pos.y, size, 0, two_pi);
    this.stroke();
    this.fill();
  };*/
  //------------------ DRAW A Ball ON CANVAS ----------------- //
  varCtx.drawBarrier = function(barrier){
    varCtx.drawImage(barrier.sprite, barrier.pos.x, barrier.pos.y, barrier.sizes.w, barrier.sizes.h);

    if(Pacman.hero.barriersCollid.some(bar => bar.timeAtBarrier && bar.num === barrier.num)){
      varCtx.beginPath();
      varCtx.strokeStyle = "#cc0000";
      varCtx.lineWidth   = 5;
      varCtx.rect(barrier.pos.x, barrier.pos.y, barrier.sizes.w, barrier.sizes.h);
      varCtx.stroke();
    }
  };
  //------------------ DRAW A BONUS ON CANVAS ----------------- //
  varCtx.drawBonus = function(bonus){
    let pos        = bonus.pos;
    let size       = bonus.size;

    varCtx.drawImage(bonus.sprite, pos.x, pos.y, size, size);
  };
  //------------------ DRAW A LINE BETWEEN TWO POINTS ON CANVAS ----------------- //
  varCtx.ln = function(x1, y1, x2, y2){
    this.moveTo(x1, y1);
    this.lineTo(x2, y2);
  };
  //------------------ DRAW A LINE BETWEEN TWO POINTS ON CANVAS ----------------- //
  varCtx.line = function(ln){
    this.beginPath()
    this.ln(ln.start.x, ln.start.y, ln.end.x, ln.end.y);
    this.stroke();
  };
  //------------------ DRAW A CROSS ON CANVAS ----------------- //
  varCtx.cross = function(point, size){
    this.ln(point.x, point.y - size, point.x, point.y + size);
    this.ln(point.x + size, point.y, point.x - size, point.y);
  };
  //------------------ DRAW A DIAG CROSS ON CANVAS ----------------- //
  varCtx.crossDiag = function(point, size){
    this.moveTo(point.x - size, point.y - size);
    this.lineTo(point.x + size, point.y + size);
    this.moveTo(point.x + size, point.y - size);
    this.lineTo(point.x - size, point.y + size);
  };
  //------------------ DRAW AN ANGLE CROSS ON CANVAS ----------------- //
  varCtx.angleCross = function(pt, size, angle){
    let points = [
      point(pt.x, pt.y - size),
      point(pt.x, pt.y + size),
      point(pt.x + size, pt.y),
      point(pt.x - size, pt.y)
    ];

    for(let i = 0; i < 4; i++){ points[i] = rotate(points[i], pt, angle); }

    this.moveTo(points[0].x, points[0].y);
    this.lineTo(points[1].x, points[1].y);
    this.moveTo(points[2].x, points[2].y);
    this.lineTo(points[3].x, points[3].y);
  };
  //------------------ DRAW MULTI ARC ON CANVAS ----------------- //
  varCtx.arcMulti = function(point, size, nb){
    for(let i = 1; i <= nb; i++){
      this.arc(point.x, point.y, size * i / nb, 0, two_pi, false);
      this.stroke();
      this.beginPath();
    }
  };
  //------------------ DRAW ELLIPSE ON CANVAS ----------------- //
  varCtx.spiral = function(pos, r, dim){
    var nb = floor(r/dim - 1);

    var angle = half_pi;
    var turn  = ceil(two_pi / angle);

    for(let i = 0; i < nb; i++){
      var less = i * dim;
      this.beginPath();
      this.ellipse(pos.x, pos.y, r - less, r - less, 0, 0, angle, false);
      this.stroke();
      for(let j = 1; j < turn; j++){
        var add = j*angle;
        this.beginPath();
        this.ellipse(pos.x, pos.y, r - dim - less, r - less, 0, add, angle + add, false);
        this.stroke();
      }
    }
  };
  //------------------ DÉSSINER UN POLYGONE ----------------- //
  varCtx.polygone = function(opt, star){
    if(opt.nb_edges < 5){ star = false; }

    let nb_edges = opt.nb_edges;
    let nbRots   = !star ? 1 : Math.floor((nb_edges+1)/2) - 1;
    let pos      = opt.pos;

    let point = {x: pos.x, y: pos.y - opt.size};

    if(opt.rot || opt.rot == 0){ point = rotate(point, pos, opt.rot); }

    this.strokeStyle = opt.color;
    this.moveTo(point.x, point.y);

    let oneMore = false;
    for(var i = 0; i < nb_edges; i++){
      if(star && nb_edges % 2 == 0 && nb_edges % 4 != 0 && i == 1 + nb_edges/2){
        oneMore = true;
        point = rotate(point, pos, two_pi/nb_edges);
        this.moveTo(point.x, point.y);
      }
      point = rotate(point, pos, nbRots * two_pi/nb_edges);
      this.lineTo(point.x, point.y);
    }

    if(oneMore){
      point = rotate(point, pos, nbRots * two_pi/nb_edges);
      this.lineTo(point.x, point.y);
    }
  };
  //------------------ DÉSSINER UNE ÉTOILE ----------------- //
  varCtx.star = function(opt){
    let nb_edges = opt.nb_edges;
    let nbRots   = Math.floor((nb_edges+1)/2) - 1;
    let pos      = opt.pos;

    let point = {x: pos.x, y: pos.y - opt.size};

    if(opt.rot || opt.rot == 0){ point = rotate(point, pos, opt.rot); }

    this.strokeStyle = opt.color;
    this.moveTo(point.x, point.y);

    for(var i = 0; i < nb_edges; i++){
      oneMore = false;
      if(nb_edges % 2 == 0 && nb_edges % 4 != 0 && i == 1 + nb_edges/2){
        oneMore = true;
        point = rotate(point, pos, two_pi/nb_edges);
        this.moveTo(point.x, point.y);
      }

      point = rotate(point, pos, nbRots*two_pi/nb_edges);
      this.lineTo(point.x, point.y);

      if(oneMore){
        point = rotate(point, pos, nbRots * two_pi/nb_edges);
        this.lineTo(point.x, point.y);
      }
    }
  };
  //------------------ DÉSSINER UN NUAGE DE POINTS ----------------- //
  varCtx.cloud = function(opt){
    let pos = opt.pos;
    let siz = opt.size;
    let nbp = opt.nb_points;
    let szp = opt.sz_point;

    this.strokeStyle = opt.color;

    if(!opt.withLine){
      for(let i = 0; i < nbp; i++){
        let pt_angle     = rnd() * two_pi;
        let pt_x = pos.x + rnd() * siz * Math.cos(pt_angle);
        let pt_y = pos.y + rnd() * siz * Math.sin(pt_angle);

        this.moveTo(pt_x, pt_y);
        this.arc(pt_x, pt_y, szp, 0, two_pi, false);
      }
    }
    else{
      for(let i = 0; i < nbp; i++){
        let pt_angle     = rnd() * two_pi;
        let pt_x = pos.x + rnd() * siz * Math.cos(pt_angle);
        let pt_y = pos.y + rnd() * siz * Math.sin(pt_angle);

        this.moveTo(pos.x, pos.y);
        this.arc(pt_x, pt_y, szp, 0, two_pi, false);
      }
    }
  };
  //------------------ DÉSSINER UNE FORME ALÉATOIRE ----------------- //
  varCtx.alea_form = function(opt){
    let nb_edges = opt.nb_edges;
    let pos      = opt.pos;

    let point = {x: pos.x, y: pos.y - opt.size};

    pos.x+=rnd()*opt.size;
    pos.y+=rnd()*opt.size;

    this.strokeStyle = opt.color;
    this.moveTo(point.x, point.y);

    for(var i = 0; i < nb_edges; i++){
      point = rotate(point, pos, two_pi/(nb_edges*rnd()));
      this.lineTo(point.x, point.y);
    }
  };
  //------------------ DÉSSINER UNE COURBE DE BÉZIER ----------------- //
  varCtx.bezier = function(startPt, endPt){
    let dx = endPt.x - startPt.x;
    let dy = endPt.y - startPt.y;

    let dAnchor  = h(dx, dy) * 0.5;

    let qPt  = {x: startPt.x + dx/4, y: startPt.y + dy/4};
    let tqPt = {x: startPt.x + 3*dx/4, y: startPt.y + 3*dy/4};

    let angle = atan2piZ(dx, dy);
    let dir   = direction(angle+half_pi, dAnchor);

    let firstAnchor = {x: qPt.x  + dir.x, y: qPt.y  + dir.y};
    let lastAnchor  = {x: tqPt.x - dir.x, y: tqPt.y - dir.y};

    this.moveTo(startPt.x, startPt.y);
    this.bezierCurveTo(firstAnchor.x, firstAnchor.y, lastAnchor.x, lastAnchor.y, endPt.x, endPt.y);
  };
  //------------------ DÉSSINER AVEC LA BROSSE ----------------- //
  varCtx.brush = function(pos, avSize, moves){
    moves.forEach(move => {
      let vector = move.vector;
      let type   = move.type;
      let size   = move.size * avSize;

      pos = {x: pos.x + vector.x*avSize, y: pos.y + vector.y*avSize};
      if(type === 'espace'){ this.moveTo(pos.x, pos.y); }
      else if(type === 'line'){
        this.lineTo(pos.x, pos.y);
      }
      else{
        switch(move.formType){
          case 'circle' : this.moveTo(pos.x + size/2, pos.y); this.arc(pos.x, pos.y, size/2, 0, two_pi); break; 
          case 'square' : this.moveTo(pos.x - size/2, pos.y - size/2); this.rect(pos.x - size/2, pos.y - size/2, size, size); break; 
        }
      }
    });
  };
  //------------------ RETURN TRUE IF PIXEL AT POS IS BLANK, ELSE FASE ----------------- //
  varCtx.isBlank = function(pos, data = imgData){
    let dataPixel = getPosInImageData(data.data, pos.x, pos.y, data.width);
    return dataPixel[0] == 0 && dataPixel[1] == 0 && dataPixel[2] == 0 && dataPixel[3] == 0;
  };

  varCtx.font = "30px Comic Sans MS";
}

function resizeAllCanvas(){
  allCanvas.forEach(canvas => { fix_dpi(canvas); });
}

/**
*@description Stroke a draw function on ctxVar
*@param {CanvasRenderingContext2D} ctxVar The ctx var
*@param {function} func The func
*/
function strokeOnCanvas(ctxVar, func){
  ctxVar.beginPath();
  func();
  ctxVar.stroke();
}

//------------------ TAILLE DU CANVAS ADAPTÉE À LA RÉSOLUTION D'ÉCRAN ----------------- //
function fix_dpi(varCanvas) {
let style = {
    height() {
      return +getComputedStyle(varCanvas).getPropertyValue('height').slice(0,-2);
    },
    width() {
      return +getComputedStyle(varCanvas).getPropertyValue('width').slice(0,-2);
    }
  };
  varCanvas.setAttribute('width', style.width() * dpi);
  varCanvas.setAttribute('height', style.height() * dpi);
}

//------------------ CLEAR CANVAS ----------------- //
function clear(){ ctx.clearRect(0, 0, canvas.width, canvas.height); /*dataCanvas = new Uint8Array(canvas.width * canvas.height);*/ }

//------------------ AfFICHE UN MESSAGE TEMPORAIRE SUR LE CANVAS ----------------- //
function msg(...txts){
  let canvasBg = canvas.style.backgroundColor;

  if(canvasBg != ""){
    ctxStructure.fillStyle = fillStyleAccordToBg(canvas, ctxStructure);
  }
  else{
    ctxStructure.fillStyle = "#223351";
  }

  ctxStructure.font = "16px Comic Sans MS";

  let pos_y = 30;
  txts.forEach(txt => {
    ctxStructure.fillText(txt, 20, pos_y);
    pos_y+=20;
  });
}

function canvasMsg(txts){
  let canvasBg = canvas.style.backgroundColor;

  if(canvasBg != ""){
    ctxStructure.fillStyle = fillStyleAccordToBg(canvas, ctxStructure);
  }
  else{
    ctxStructure.fillStyle = "#223351";
  }

  if(Array.isArray(txts)){
    txts.forEach(txt => {
      ctxStructure.font      = txt.font ? txt.font : "16px Comic Sans MS";
      ctxStructure.fillStyle = txt.fillStyle ? txt.fillStyle : ctxStructure.fillStyle;

      ctxStructure.fillText(txt.text, txt.pos.x, txt.pos.y);
    });
  }
  else{
    ctxStructure.font = txts.font ? txts.font : "16px Comic Sans MS";
    ctxStructure.fillText(txts.text, txts.pos.x, txts.pos.y);
  }
}

function fillStyleAccordToBg(canvasVar, ctxVar){
  ctxVar.fillStyle = objRgb_to_strRgb(updateColorToBack(strRgb_to_objRgb(canvasVar.style.backgroundColor)));
}

//------------------ MODIFICATION DE L'IMAGE ----------------- //
function updImage(func, simple = true, clear = false){
  var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
  var data = simple ? imageData.data : arrColors(imageData.data);

  if(typeof(func) == 'function'){ func(data); }

  if(!simple){ simpleArr(data, imageData.data); }

  if(clear){ ctx.clearRect(0, 0, canvas.width, canvas.height); }

  ctx.putImageData(imageData, 0, 0);
}
function simpleUpdImage(ctxVar, func, arg){
  var imageData = ctxVar.getImageData(0, 0, canvas.width, canvas.height);

  if(typeof(func) == 'function'){ func(arg); }

  ctxVar.putImageData(imageData, 0, 0);
}

/**
 * @description Get property of a css rule by id or class
 * @param {string} classOrId class or id of the css rule
 * @param {string} property the property to return the value in the css rule
 * @returns {void}
 */
function getStyleProperty(classOrId, property)
{
    let firstChar = classOrId.charAt(0);
    let remaining = classOrId.substring(1);
    let elem      = (firstChar =='#') ?  document.getElementById(remaining) : document.getElementsByClassName(remaining)[0];

    return window.getComputedStyle(elem, null).getPropertyValue(property);
}

//------------------ TURN DATA IMAGE TO ARRAY COLORS ----------------- //
function arrColors(data){
  arrayColors = []; dataLength = data.length;
  for (var i = 0; i < dataLength; i += 4) {
    arrayColors.push({r: data[i], g: data[i + 1], b: data[i + 2], a: data[i + 3]});
  }
  return arrayColors;
}
//------------------ TURN ARRAY COLORS TO DATA IMAGE ----------------- //
function simpleArr(data, imgData){
  n = 0;
  data.forEach(color => {
    imgData[n] = color.r; imgData[n + 1] = color.g; imgData[n + 2] = color.b; imgData[n + 3] = color.a;
    n+=4;
  });
}


//------------------ CALCUL ANGLE BETWEEN 0 & 2PI ----------------- //
function twoPiAngle(pos, center){
  let posToCenter = {x: pos.x - center.x, y: pos.y - center.y};
  let angle = atan(posToCenter.x/posToCenter.y);

  let pos_y = posToCenter.y < 0 ? true : false;
  let pos_x = posToCenter.x < 0 ? true : false;

  if    (!pos_x && pos_y)  { angle = half_pi + angle; }
  else if(pos_x && pos_y)  { angle = angle + half_pi; }
  else                     { angle = 3*half_pi + angle; }

  return angle;
}

function getRandomPoint(coeff){ return {x: canvas.width * (coeff * rnd() + (1-coeff)/2), y: canvas.height * (coeff * rnd() + (1-coeff)/2)}; }

function makeDialog(options = {style: {width: '50%', height: '50%'}, }, content, closeOrRemove = 'remove'){
  let dialogContainer = getById('dialogContainer');

  let dialog = document.createElement("dialog");

  for (let prop in options.style){
    dialog.style[prop] = options.style[prop];
  }

  dialog.id = options.id ? options.id : '';

  dialog.style.position     = 'absolute';
  dialog.style.border       = 'none';
  dialog.style.borderRadius = '5px';
  dialog.style.overflowX    = 'hidden';

  let numId = parseInt(rnd() * 32000); 

  let opInput = "<div>";
  opInput += "<label for='dialogOpacity_" + numId + "'>Opacité</label>";
  opInput += "<input type='range' id='dialogOpacity_" + numId + "' name='dialogOpacity_" + numId + "' class='input_help'";
  opInput += " oninput='this.parentElement.parentElement.parentElement.style.opacity = this.value;  ' onchange='event.stopPropagation(); ' onclick='event.stopPropagation(); '";
  opInput += " min='0.01' max='1' value='0.67' step='.01'>"
  opInput += "</div>";

  content = "<div class='dialogContentContainer' onclick='event.stopPropagation(); '>" + opInput +  content + "</div>";

  dialog.addEventListener('click', (e) => {
    closeOrRemove === 'remove' ? dialog.remove() : dialog.close();
  });

  dialog.innerHTML = content;
  dialogContainer.appendChild(dialog);

  return dialog;
}

function makeInfosArrObjsDialog(arrObjs, isSorted = false, newDir = 'none', idDial = false, title){
  if(typeof arrObjs === 'string'){
    arrObjs = arrObjs.replaceAll('///', '\"');
    arrObjs = JSON.parse(arrObjs);
  }

  let idDialog = !idDial ? ('infosDialog_' + parseInt(rnd() * 32000)) : idDial;

  let infsObjs = infosArrObjs(arrObjs, isSorted, newDir);

  let arrObjsString = JSON.stringify(arrObjs);
  arrObjsString = arrObjsString.replace(/\"/g, '///');

  let limNbProps = 20;
  infsObjs.propsInObjs = infsObjs.propsInObjs.slice(0, limNbProps);
  infsObjs.infosObjs.forEach((_infObj, i) => { infsObjs.infosObjs[i] = infsObjs.infosObjs[i].slice(0, limNbProps); });

  let content   = "<h1 style='text-align: center; '>" + title + "</h1>";
  content      += "<table style='width: 100%; border-collapse: collapse; '>";
  content      += "<thead style='border-bottom: 1px #ccc solid; '>";
  content      += "<tr>";
  infsObjs.propsInObjs.forEach(propInObj => { content += `<th class="thHelpInfo sort_${isSorted === propInObj ? newDir : 'none' }" ${isSorted === propInObj ? "data-sortdir='" + newDir + "' " : "data-sortdir='none' " } onclick="makeInfosArrObjsDialog('${arrObjsString}', this.textContent, this.dataset.sortdir !== 'none' && this.dataset.sortdir === 'asc' ? 'desc' : 'asc', '${idDialog}', '${title}'); ">${propInObj}</th>`; });
  content      += "</tr>";
  content      += "</thead>";
  content      += "<tbody style='text-align: center; '>";
  infsObjs.infosObjs.forEach(infObj => {
    content += "<tr onclick='trSelect(this); ' onmouseenter=\"addClasses(this, 'flyOnTr'); \" onmouseleave=\"removeClasses(this, 'flyOnTr'); \">";
    infObj.forEach( infOb => {
      let val = typeof infOb.val === 'number' ? round(infOb.val, 2) : infOb.val;
      content += `<td>${val}</td>`;
    });
    content += "</tr>";
  });
  content      += "</tbody>";
  content      += "</table>";

  if(!isSorted){
    let infosDialog = makeDialog(options = {style: {width: '95%', height: '77%'}, id: idDialog}, content);
    return infosDialog;
  }

  content = "<div class='dialogContentContainer' onclick='event.stopPropagation(); '>" + content + "</div>";
  getById(idDialog).innerHTML = content;
  return content;
}

function infosArrObjs(arrObjs, isSorted, newDir){
  let infosObjs   = [];
  let propsInObjs = allInfosArr(arrObjs[0]).map(p => p.prop);

  if(propsInObjs){
    arrObjs.forEach(obj => { infosObjs.push(allInfosArr(obj, propsInObjs)); });
    if(isSorted){ sortInfosArray(infosObjs, isSorted, newDir); }
    return {infosObjs, propsInObjs};
  }
  return false;
}

function allInfosArr(obj, propsInObj = false){
  if(obj){
    let props = [];
    for(let prop in obj){
      if((!propsInObj || propsInObj.includes(prop)) && (typeof obj[prop] !== 'object' && typeof obj[prop] !== 'function')){
        let val = typeof obj[prop] !== 'object' ? obj[prop] : JSON.stringify(obj[prop]);
        props.push({prop: prop, val: val, typeof: typeof obj[prop]});
      }
    }
    return props;
  }
  return [];
}

function trSelect(tr){
  !tr.classList.contains('trSelect') ? addClasses(tr, 'trSelect') : removeClasses(tr, 'trSelect');
}

function infosArr(obj, propsInObj = false){
  if(obj){
    let props = [];
    for(let prop in obj){
      if(typeof obj[prop] !== 'object' && typeof obj[prop] !== 'function'){
        if(!propsInObj || propsInObj.includes(prop)){ props.push({prop: prop, val: obj[prop]}); }
      }
    }
    return props;
  }
  return [];
}

function sortInfosArray(infosMods, prop, dir = 'asc'){
  let numProp = 0;
  for(let i = 0; i < infosMods[0].length; i++){
    if(infosMods[0][i].prop === prop){ numProp = i; break; }
  }
  return dir === 'asc' ?
                       infosMods.sort((arr1, arr2) => {
                        let val1 = typeof arr1[numProp].val !== 'boolean' ? arr1[numProp].val : (arr1[numProp].val ? 1 : 0);
                        let val2 = typeof arr2[numProp].val !== 'boolean' ? arr2[numProp].val : (arr2[numProp].val ? 1 : 0);

                        return parseFloat(val1) - parseFloat(val2);
                       }) :
                       infosMods.sort((arr1, arr2) => {
                        let val1 = typeof arr1[numProp].val !== 'boolean' ? arr1[numProp].val : (arr1[numProp].val ? 1 : 0);
                        let val2 = typeof arr2[numProp].val !== 'boolean' ? arr2[numProp].val : (arr2[numProp].val ? 1 : 0);

                        return parseFloat(val2) - parseFloat(val1);
                       }) 
}

function toggleArrObjsDialog(arrObjs, title){
  let infosObjsDialog = getById('infosObjsDialog');
  if(infosObjsDialog){ infosObjsDialog.remove(); }
  else{
    let infosObjsDialog = makeInfosArrObjsDialog(arrObjs, false, 'none', false, title);
    infosObjsDialog.addEventListener("close", (_event) => {
    });
    infosObjsDialog.showModal();
  }
}


function test_save(){
  ctx.drawHero();
}
function test(){
  ctx.beginPath();
  ctx.lineWidth   = 1;
  ctx.strokeStyle = '#cc0000';
  ctx.fillStyle   = '#000000';

  let r      = 100;
  let steps  = 12;
  let center = canvas.getCenter();

  ctx.arc(center.x, center.y, 5, 0, two_pi);
  ctx.fill();

  ctx.beginPath();
  ctx.fillStyle = '#cc0000';

  let n = 0;
  for(let i = 0; i <= two_pi; i+=two_pi/steps){
    let addVect = direct(i, r);
    let newPos = {x: center.x + addVect.x, y: center.y + addVect.y};

    let angleWithATan2Pi = atan2piZ(newPos.x - center.x, newPos.y - center.y);
    canvasMsg([
      {
        pos:  {x: newPos.x, y: newPos.y},
        text: n + " x 2 PI / " + steps,
        font    : "bold 12px serif",
      },
      {
        pos:  {x: newPos.x + 10, y: newPos.y + 17},
        text: round(angleWithATan2Pi, 2),
        font    : "bold 12px serif",
      },
    ]);
    n++;
  }

  function direct(angle, dist){
    return {
      x: cos(angle) * dist,
      y: sin(angle) * dist,
    }
  }
  function atanPi2(x, y){
    let angle = Math.atan2(x, y);
    return angle > 0 ? angle : (two_pi + angle);
  }
}



















//END
