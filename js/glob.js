let throwBallTimeOut     = 0;
let throwBallTimeOutLast = 0;

let testApp    = false;
let stopBadGuy = false;

let lapsGame     = 0;
let level        = 1;
let nextLevel    = false;
let game_over    = false;
let soundOn      = true;
let bonus        = [];

let getById = function(id){ return document.getElementById(id); };

let int   = parseInt;
let rnd   = Math.random;
let abs   = Math.abs;
let pow   = Math.pow;
let sqr   = Math.sqrt;
let cos   = Math.cos;
let sin   = Math.sin;
let tan   = Math.tan;
let atan  = Math.atan;
let atan2 = Math.atan2;
let floor = Math.floor;
let ceil  = Math.ceil;
let hypo  = function(a,b){ return pow(pow(a,2)+pow(b,2), 0.5); };
let h     = hypo;

const PI       = Math.PI;
const two_pi   = 2*PI;
const half_pi  = PI/2;
const quart_pi = PI/4;
const rad      = PI/180;

const SQRT2   = Math.SQRT2;
const SQRT1_2 = Math.SQRT1_2;

let requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

let cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame;

let canvas       = getById('arene');
let ctx          = canvas.getContext('2d');
let structure    = getById('arene');
let ctxStructure = canvas.getContext('2d');

let allCanvas = [canvas, structure];

let mouseDown = false;
let mouseUp   = false;
let dpi       = window.devicePixelRatio;
let pause     = false;
let mouse     = { click: {x: 0, y: 0} };

let audioCtx = new AudioContext();

let imgData;
let objectUrl;