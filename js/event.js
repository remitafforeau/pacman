//------------------ LANCEMENT DE LA BOUCLE DE DESSIN ----------------- //
document.addEventListener("DOMContentLoaded", function() {
  //pickSprites();
  resizeAllCanvas();
  initLevel(); 
  animation();
});

//------------------ ClICK SUR LE CANVAS POUR SWITCHER L'AFFICHAGE DES PARAMÉTRES ----------------- //
structure.addEventListener('click', () => {
  
});
//------------------ ClICK DROIT SUR LE CANVAS POUR MODIFIER DES PARAMÉTRES ----------------- //
structure.addEventListener('contextmenu', (e) => {
  e.preventDefault();
});
//------------------ MOUSE MOVEMENT ----------------- //
structure.addEventListener('mousedown', (e) => {
  mouseUp = false;
  if(e.button !== 2){
    mouseDown = true;
    Pacman.hero.rot = atan2piZ(mouse.x - Pacman.hero.pos.x, mouse.y - Pacman.hero.pos.y);
    Pacman.hero.pos = {x: mouse.x, y: mouse.y};
  }
  else{
    Pacman.hero.throwBallInMouseDir();
  }
});
structure.addEventListener('mouseup',   () => {
  mouseDown = false;
  mouseUp   = true;
});
structure.addEventListener('wheel', (e) => {
  e.preventDefault();
});
structure.addEventListener('mousemove', (e) => {
  mouse = getMousePos(e, canvas, mouse);

  if(mouseDown){
    let heroBarrierCollision = Pacman.hero.barriersCollision();
    if(!heroBarrierCollision){
      //Pacman.hero.timeAtBarrier = 0;

      Pacman.hero.rot = atan2piZ(mouse.x - Pacman.hero.pos.x, mouse.y - Pacman.hero.pos.y);
      Pacman.hero.pos = {x: mouse.x, y: mouse.y};
    }
    else{
      //if(!Pacman.hero.timeAtBarrier){ Pacman.hero.timeAtBarrier = getTime(); }
      //Pacman.hero.timeInBarrier = getTime() - Pacman.hero.timeAtBarrier;

      Pacman.hero.rot = atan2piZ(mouse.x - Pacman.hero.pos.x, mouse.y - Pacman.hero.pos.y);
      Pacman.hero.pos = {x: mouse.x, y: mouse.y};
    }
  }
});

//------------------ RESIZE CANVAS WHEN RESIZE WINDOW ----------------- //
window.addEventListener('resize', function () {
  resizeAllCanvas();
});

//------------------ ÉVÈNEMENTS D'APPUI SUR UNE TOUCHE ----------------- //
window.addEventListener("keydown", function (e) {
  if(e.key != 'Shift'){
    if(e.key !== 'F12' && e.key !== 'f'){ e.preventDefault(); }

    let key = e.key;
    if(isNaN(parseInt(key))){
      if(!e.ctrlKey){
        if(!e.altKey){
        	switch (key) {
            /// ⏎ -- Pause -- game -- pause ///
        		case 'Enter':
              pause = !pause;
        			break;
            /// b -- Badguy -- game ///
        		case 'b':
              toggleArrObjsDialog(Ball.balls, "BALLS");
              break;
            /// c -- Barrier -- game ///
        		case 'c':
              toggleArrObjsDialog(Barrier.barriers, "BARRIÈRE");
        			break;
            /// d -- stopBadGuy -- stopBadGuy ///
        		case 'd':
              stopBadGuy = !stopBadGuy;
        			break;
            /// n -- Pacman -- game ///
        		case 'n':
              toggleArrObjsDialog([Pacman.hero], "HERO");
        			break;
            /// q -- Pass level -- level -- level ///
        		case 'q':
              if(!nextLevel){ location.reload(); }
              else{
                initLevel(++level);
              }
        			break;
            /// t -- Test -- test -- test ///
        		case 't':
              testApp = !testApp;
        			break;
            /// s -- Mute -- sound -- soundOn ///
        		case 's':
              soundOn = !soundOn;
        			break;
            /// v -- Badguy -- game ///
        		case 'v':
              toggleArrObjsDialog(Pacman.badguys, "BADGUYS");
        			break;
            /// w -- Dead -- game ///
        		case 'w':
              Pacman.hero.size = -999;
        			break;
            /// x -- Bonus -- game ///
        		case 'x':
              toggleArrObjsDialog(Bonus.bonus, "BONUS");
        			break;
            /// < -- Win -- game ///
        		case '<':
              Pacman.badguys = [];
        			break;
            /// Esc -- Rechargement de la page -- divers ///
        		case 'Escape':
              if(!nextLevel){ location.reload(); }
              else{
                initLevel(++level);
              }
        			break;
            /// Esp -- Lancce une balle -- divers ///
        		case ' ':
              if(e.repeat){
                throwBallTimeOutLast = e.timeStamp;
                if(throwBallTimeOutLast - throwBallTimeOut > Pacman.hero.BurstFireSpeed){
                  Pacman.hero.throwBallInPacmanDir();
                  throwBallTimeOut = e.timeStamp;
                }
              }
              else{
                throwBallTimeOut = e.timeStamp;
                Pacman.hero.throwBallInPacmanDir();
              }
        			break;
            /// ArrowLeft -- Badguy -- game ///
        		case 'ArrowLeft':
              Pacman.hero.rot -= 2 * rad;;
        			break;
            /// ArrowRight -- Badguy -- game ///
        		case 'ArrowRight':
              Pacman.hero.rot += 2 * rad;;
        			break;
        	}
        }
        else{
          e.preventDefault();
          switch (key) {
          }
        }
      }
      else{
        if(key != 'f'){ e.preventDefault(); }
        if(e.shiftKey){
          switch (key) {
            case 'ArrowLeft':
              break;
            case 'ArrowRight':
              break;
          }
        }
        else{
          switch (key) {
          }
        }
      }
    }
    else{
      if(!e.ctrlKey){
        let numKey = parseInt(key);
      }
      else{
        e.preventDefault();
        switch (key) {
        }
      }
    }
  }
});




//END
